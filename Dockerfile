FROM ruby:2.5.4-stretch

MAINTAINER Torchlight team <devops@torchlight.care>

RUN apt-get update && apt-get install -qq -y --no-install-recommends \
   build-essential git-core zip unzip

ENV INSTALL_PATH /torchlight

RUN mkdir -p $INSTALL_PATH

WORKDIR $INSTALL_PATH
